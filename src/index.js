const express = require("express");
const morgan = require("morgan");
const mongoose = require("mongoose");

const { authRouter } = require("./routes/authRouter");
const { userRouter } = require("./routes/userRouter");
const { truckRouter } = require("./routes/truckRouter");
const { loadsRouter } = require("./routes/loadsRouter");

const { authMiddleware } = require("./middleware/authMiddleware");
const { errorHandler } = require("./middleware/errorMiddleware");
const { driverGuardMiddleware } = require("./middleware/driverGuardMiddleware");
const { asyncWrapper } = require("./helpers/asyncWrapper");

require("dotenv").config();

const app = express();

mongoose.connect(process.env.MONGODB_CONNECTION);

app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);
app.use("/api/users/me", asyncWrapper(authMiddleware), userRouter);
app.use(
  "/api/trucks",
  asyncWrapper(authMiddleware),
  driverGuardMiddleware,
  truckRouter
);
app.use("/api/loads", asyncWrapper(authMiddleware), loadsRouter);

(async () => {
  try {
    app.listen(process.env.PORT || 8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
})();

// ERROR HANDLER
app.use(errorHandler);
