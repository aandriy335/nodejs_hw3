const { Types, Schema, model } = require("mongoose");

const Joi = require("joi");

const { LOAD_STATES, LOAD_STATUSES } = require("../constants");

const loadJoiSchema = Joi.object().keys({
  status: Joi.string()
    .custom((status, helper) =>
      LOAD_STATUSES.includes(status)
        ? status
        : helper.message(
            `Status must be defined as some of these values: ${LOAD_STATUSES.join(
              ", "
            )}`
          )
    )
    .optional(),
  state: Joi.string()
    .custom((state, helper) =>
      LOAD_STATES.includes(state)
        ? state
        : helper.message(
            `State must be defined as some of these values: ${LOAD_STATES.join(
              ", "
            )}`
          )
    )
    .optional(),
  name: Joi.string().min(2).required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }).required(),
});

const loadSchema = new Schema({
  created_by: {
    type: Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: Types.ObjectId,
    default: Types.ObjectId,
    required: true,
    turnOn: true,
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(LOAD_STATUSES),
    default: "NEW",
  },
  state: {
    type: String,
    required: true,
    enum: LOAD_STATES,
    default: "Arrived to Pick Up",
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
        required: true,
      },
      time: {
        type: String,
        default: new Date().toISOString(),
        required: true,
      },
    },
  ],
  created_date: {
    type: String,
    default: new Date().toISOString(),
    required: true,
  },
});

const Load = model("loads", loadSchema);

module.exports = {
  Load,
  loadJoiSchema,
};
