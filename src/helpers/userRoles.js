/**
 * @param {["DRIVER", "SHIPPER"]} role
 */
const isShipperRole = (role) => role === "SHIPPER";

/**
 * @param {["DRIVER", "SHIPPER"]} role
 */
const isDriverRole = (role) => role === "DRIVER";

module.exports = {
  isDriverRole,
  isShipperRole
};
