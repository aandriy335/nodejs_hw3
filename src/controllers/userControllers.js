/* eslint-disable camelcase */
const bcrypt = require("bcryptjs");

const { User } = require("../models/userModel");

require("dotenv").config();

const getUserProfile = async (req, res) => {
  const { _id, role, email, created_date } = req.currentUserProfile;

  return res.status(200).json({
    user: {
      _id,
      role,
      email,
      created_date
    }
  });
};

const deleteUserProfile = async (req, res) => {
  const { _id } = req.currentUserProfile;

  const userProfile = await User.findByIdAndDelete(_id);

  console.log(userProfile);

  return res.status(200).json({ message: "Profile deleted successfully" });
};

const updateUserPassword = async (req, res) => {
  const { _id, password } = req.currentUserProfile;
  const { oldPassword, newPassword } = req.body;

  const currentUserProfile = await User.findById(_id);

  if (!bcrypt.compareSync(oldPassword, password)) {
    return res.status(400).json({ message: "Your old password isn't correct" });
  }

  if (!newPassword) {
    return res.status(400).json({ message: "New password can't be empty" });
  }

  const encodedNewPassword = bcrypt.hashSync(
    newPassword,
    bcrypt.genSaltSync(+process.env.ENCODE_ROUNDS)
  );

  currentUserProfile.password = encodedNewPassword;
  currentUserProfile.save().then((updatedProfile) => {
    console.log(updatedProfile);
  });

  return res.status(200).json({ message: "Password changed successfully" });
};

module.exports = {
  getUserProfile,
  deleteUserProfile,
  updateUserPassword
};
