/* eslint-disable camelcase */
const mongoose = require("mongoose");

const { loadJoiSchema, Load } = require("../models/loadModel");
const { Truck } = require("../models/truckModel");
const { User } = require("../models/userModel");

const { switchLoadState } = require("../helpers/switchLoadState");
const { switchLoadStatus } = require("../helpers/switchLoadStatus");
const { loadIsFit } = require("../helpers/loadIsFit");
const { isShipperRole, isDriverRole } = require("../helpers/userRoles");

const { LOAD_STATUSES, TRUCK_STATUSES } = require("../constants");

const getUserLoads = async (req, res) => {
  const { _id: userId, role } = req.currentUserProfile;

  console.log(userId, role);

  let loads = null;

  if (isDriverRole(role)) {
    loads = await Load.find(
      {
        $or: [
          { assigned_to: userId },
          { status: [LOAD_STATUSES.ASSIGNED, LOAD_STATUSES.SHIPPED] },
        ],
      },
      "-__v -logs._id"
    );
  } else if (isShipperRole(role)) {
    loads = await Load.find({ created_by: userId }, "-__v -logs._id");
  }

  if (loads.length === 0) {
    return res.status(400).json({
      message: `Loads for a user with id: ${userId} is not found`,
    });
  }

  return res.status(200).json({
    loads,
  });
};

const createLoad = async (req, res) => {
  const { _id: userId } = req.currentUserProfile;
  const { name, payload, pickup_address, delivery_address, dimensions } =
    req.body;

  await loadJoiSchema.validateAsync({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  });

  const load = new Load({
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
    created_by: userId,
  });

  const savedLoad = await load.save();

  console.log(savedLoad);

  return res.status(200).json({ message: "Load created successfully" });
};

const getActiveLoad = async (req, res) => {
  const { _id: driverId } = req.currentUserProfile;

  console.log(driverId);

  const activeLoad = await Load.findOne(
    {
      assigned_to: driverId,
    },
    "-__v -logs._id"
  );

  console.log(activeLoad);

  if (!activeLoad) {
    return res.status(400).json({
      message: `No active load for driver ${driverId} is not found`,
    });
  }

  return res.status(200).json({
    load: activeLoad,
  });
};

const nextActiveState = async (req, res) => {
  const { _id: userId } = req.currentUserProfile;

  const activeState = await Load.findOne({ assigned_to: userId });

  if (!activeState) {
    return res.status(400).json({
      message: "No active load is not found",
    });
  }

  const nextState = switchLoadState(activeState.state);

  if (nextState === activeState.state) {
    return res.status(400).json({ message: "Load has already SHIPPED state" });
  }

  activeState.logs.push({
    message: `Load state changed to '${nextState}'`,
  });
  activeState.state = nextState;

  if (nextState === "Arrived to delivery") {
    activeState.status = switchLoadStatus(activeState.status);
    activeState.logs.push({
      // eslint-disable-next-line no-underscore-dangle
      message: `Load with id: ${activeState._id} is SHIPPED`,
    });

    const truck = await Truck.findOne({ assigned_to: activeState.assigned_to });

    truck.status = "IS";

    await truck.save();
    await activeState.save();
  }

  await activeState.save();

  console.log(activeState);

  return res.status(200).json({
    message: `Load state changed to '${nextState}'`,
  });
};

const postUserLoadById = async (req, res) => {
  const { id } = req.params;

  const currentLoad = await Load.findById(id);

  if (!currentLoad) {
    return res.status(400).json({
      message: `Load with id: ${id} is not found`,
    });
  }

  if (currentLoad.status === LOAD_STATUSES.POSTED) {
    return res
      .status(400)
      .json({ message: `Load with id: ${id} is already posted` });
  }

  currentLoad.status = switchLoadStatus(currentLoad.status);

  const trucks = await Truck.find({ status: "IS" });
  const firstTruck = trucks.filter((truck) =>
    loadIsFit(truck.type, currentLoad.dimensions, currentLoad.payload)
  )[0];

  if (!firstTruck) {
    currentLoad.status = LOAD_STATUSES.NEW; // reset load status

    currentLoad.logs.push({
      message: `There is no found truck to deliver load '${currentLoad.name}'`,
    });
    await currentLoad.save();

    return res.status(400).json({
      message: `There is no found truck to deliver load '${currentLoad.name}'`,
    });
  }

  const driverId = firstTruck.assigned_to;
  const assignedDriver = await User.findOne({ _id: driverId, role: "DRIVER" });

  if (!assignedDriver) {
    currentLoad.status = LOAD_STATUSES.NEW; // reset load status

    currentLoad.logs.push({
      message: `There is no found assigned driver to deliver load '${currentLoad.name}'`,
    });
    await currentLoad.save();

    return res.status(400).json({
      message: `There is no found assigned driver to deliver load '${currentLoad.name}'`,
    });
  }

  currentLoad.state = switchLoadState(currentLoad.state);
  currentLoad.status = switchLoadStatus(currentLoad.status);
  currentLoad.assigned_to = driverId;
  currentLoad.logs.push({
    message: `Load assigned to driver with id ${driverId}`,
  });

  // change assigned truck status to 'On load'
  firstTruck.status = TRUCK_STATUSES.OL;

  await firstTruck.save();
  await currentLoad.save();

  console.log(currentLoad);

  return res.status(200).json({
    message: "Load posted successfully",
    driver_found: true,
  });
};

const getUserLoadById = async (req, res) => {
  const { id } = req.params;

  const userLoad = await Load.findById(id, "-__v -logs._id");

  console.log(userLoad);

  if (!userLoad) {
    return res.status(400).json({
      message: `No load with id: ${id} is found`,
    });
  }

  return res.status(200).json({
    load: userLoad,
  });
};

const updateUserLoadById = async (req, res) => {
  const { id } = req.params;
  const { name, payload, delivery_address, pickup_address, dimensions } =
    req.body;

  await loadJoiSchema.validateAsync({
    name,
    payload,
    delivery_address,
    pickup_address,
    dimensions,
  });

  const userLoad = await Load.findById(id);

  if (userLoad.status !== LOAD_STATUSES.NEW) {
    return res.status(400).json({
      message: `User is able to update load only with status 'NEW'`,
    });
  }

  await userLoad
    .updateOne({
      name,
      payload,
      delivery_address,
      pickup_address,
      dimensions,
    })
    .then((savedLoad) => savedLoad);

  if (!userLoad) {
    return res.status(400).json({
      message: `No load with id: ${id} is found`,
    });
  }

  return res.status(200).json({
    message: "Load details changed successfully",
  });
};

const deleteUserLoadById = async (req, res) => {
  const { id } = req.params;

  const userLoad = await Load.findById(id);

  if (userLoad.status !== LOAD_STATUSES.NEW) {
    return res.status(400).json({
      message: `User is able to delete load only with status 'NEW'`,
    });
  }

  if (!userLoad) {
    return res.status(400).json({
      message: `Load with id: ${id} has been already removed`,
    });
  }

  await userLoad.delete();

  return res.status(200).json({
    message: "Load deleted successfully",
  });
};

const getLoadShippingInfoById = async (req, res) => {
  const { id } = req.params;

  const currentLoad = await Load.findById(id);

  console.log(currentLoad);

  const driverId = currentLoad.assigned_to;

  const shippingInfo = await Load.aggregate([
    {
      $match: {
        assigned_to: new mongoose.Types.ObjectId(driverId),
      },
    },
    {
      $addFields: {
        load: "$$ROOT",
      },
    },
    {
      $lookup: {
        from: "trucks",
        localField: "assigned_to",
        foreignField: "assigned_to",
        as: "truck",
      },
    },
    {
      $project: {
        _id: 0,
        load: 1,
        truck: {
          $first: "$truck",
        },
      },
    },
    {
      $project: {
        load: {
          __v: 0,
          "logs._id": 0,
        },
        truck: {
          __v: 0,
        },
      },
    },
  ]);

  console.log(shippingInfo);

  return res.status(200).json(shippingInfo[0]);
};

module.exports = {
  getUserLoads,
  createLoad,
  nextActiveState,
  postUserLoadById,
  getActiveLoad,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  getLoadShippingInfoById,
};
